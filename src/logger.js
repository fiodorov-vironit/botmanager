const winston = require('winston');

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      level: process.env.NODE_ENV !== 'production' ? 'debug' : 'info',
    }),
    new winston.transports.File({
      level: 'error',
      filename: 'bot_errors.log',
    }),
  ],
});

module.exports = logger;
