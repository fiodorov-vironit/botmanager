const redis = require('redis');
const { getLanguages } = require('../services');
const logger = require('../logger');

const storeData = {
  wakeupIntervals: {},
  users: {},
  languages: {},
  training: {},
};

const redisClient = redis.createClient(
  process.env.REDIS_PORT,
  process.env.REDIS_HOST,
);
redisClient.auth(process.env.REDIS_TOKEN);

function transformData(data) {
  return data.reduce((result, { full_code: langCode, englishName: langName }) => ({
    ...result,
    [langCode]: langName,
  }), {});
}

function syncLanguages(callback) {
  redisClient.get('languages', (data) => {
    if (!data) {
      getLanguages((err, res) => {
        if (err) {
          logger.error(err);
        }
        redisClient.set('languages', JSON.stringify(res));
        callback(transformData(res));
      });
    } else {
      callback(transformData(data));
    }
  });
}

syncLanguages((languages) => {
  storeData.languages = languages;
});

module.exports = storeData;
