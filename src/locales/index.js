/* eslint-disable camelcase */
const en_GB = require('./en_GB');
const ru_RU = require('./ru_RU');

const locales = { en_GB, ru_RU };
const defaultLocale = locales[process.env.DEFAULT_LOCALE];

function getParamNamesFromString(str) {
  const reParamNames = /\{\{(.*?)\}\}/g;
  return (str.match(reParamNames) || [])
    .map(match => new RegExp(reParamNames).exec(match)[1]);
}

function validParams(requiredParams, params) {
  return requiredParams.every(paramName => !!params[paramName]);
}

exports.t = function t(localeName, stringName, params) {
  const currentLocale = locales[localeName]
    ? locales[localeName]
    : defaultLocale;
  const templateString = currentLocale[stringName]
    ? currentLocale[stringName]
    : defaultLocale[stringName];
  const requiredParamNames = getParamNamesFromString(templateString);
  if (validParams(requiredParamNames, params)) {
    return requiredParamNames.reduce(
      (resultString, paramName) => resultString.replace(`{{${paramName}}}`, params[paramName]),
      templateString,
    );
  }
  return templateString;
};
