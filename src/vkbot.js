const VkBot = require('node-vk-bot-api');
const { commands } = require('./config/commands');
const appSettingsMiddleware = require('./middlewares/app-settings.middleware');
const userMiddleware = require('./middlewares/user.middleware');
const localeMiddleware = require('./middlewares/locale.middleware');
const wakeupMiddleware = require('./middlewares/wakeup.middleware');
const userInputMiddleware = require('./middlewares/user-input.middleware');
const reminderMiddleware = require('./middlewares/reminder.middleware');

const bot = new VkBot(process.env.API_KEY);

bot.use(appSettingsMiddleware);
bot.use(userMiddleware);
bot.use(localeMiddleware);
bot.use(wakeupMiddleware);

Object.values(commands)
  .forEach((command) => {
    bot.command(command.text, command.handler, reminderMiddleware);
  });

bot.on(userInputMiddleware, reminderMiddleware);

bot.startPolling();
