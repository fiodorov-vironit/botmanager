function objectToUrl(obj) {
  return Object.keys(obj)
    .map(prop => `${encodeURIComponent(prop)}=${encodeURIComponent(obj[prop])}`)
    .join('&');
}

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    // eslint-disable-next-line no-bitwise
    const r = Math.random() * 16 | 0;
    // eslint-disable-next-line no-bitwise
    const v = c === 'x' ? r : ((r & 0x3) | 0x8);
    return v.toString(16);
  });
}

function daysToMs(days) {
  return days * 1000 * 60 * 60 * 24;
}

function getInt(value) {
  const parsed = parseInt(value, 10);
  if (Number.isNaN(parsed)) {
    return value;
  }
  return parsed;
}

module.exports = {
  objectToUrl,
  uuidv4,
  daysToMs,
  getInt,
};
