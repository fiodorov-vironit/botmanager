module.exports = {
  connectionLimit: 100,
  waitForConnections: true,
  queueLimit: 0,
  acquireTimeout: 5000,
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  dataStrings: true,
  database: process.env.MYSQL_DB,
};
