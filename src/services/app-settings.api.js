const mysql = require('mysql');
const config = require('./app-settings.config');

const mysqlPool = mysql.createPool(config);

function handleQueryResult(callback) {
  return (err, result) => {
    if (err) {
      callback(err, result);
      return;
    }
    const settings = result.reduce((o, row) => ({ ...o, [row.name]: row.value }), {});
    callback(null, settings);
  };
}

function fetchLatestSettings(callback) {
  mysqlPool.getConnection((err, connection) => {
    if (err) {
      callback(err, connection);
      return;
    }
    connection.query(
      `SELECT value, name FROM Parameters WHERE Parameters.appId = ${process.env.APP_ID}`,
      handleQueryResult(callback),
    );
  });
}

module.exports = {
  fetchLatestSettings,
};
