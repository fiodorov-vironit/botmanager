module.exports = {
  baseURL: 'https://backenster.com',
  translateURL: '/v2/api/v3/translate?platform=dp',
  getLanguagesListURL: '/v2/api/v3/getLanguages?platform=dp&all=true',
  getLexicalMeaningURL: '/v2/api/v3/getLexicalMeaning',
  proposalURL: '/api/feedBack/addFeedBack',
  sendAddUserSubscriptionURL: '/api/subscribedUsers/addUser',
};
