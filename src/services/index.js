const backensterApi = require('./backenster.api');
const piwikApi = require('./piwik.api');
const { fetchLatestSettings } = require('./app-settings.api');

function withReport(length, eventName, from, callback) {
  return (err, res) => {
    if (process.env.NODE_ENV === 'production') {
      piwikApi.sendPiwikReport(length, eventName, from, () => callback(err, res));
    } else {
      callback(err, res);
    }
  };
}

module.exports = {
  ...backensterApi,
  translate: (text, user, cb) => {
    backensterApi.translate(
      text,
      user.from,
      user.to,
      withReport(text.length, 'translate', user.locale, cb),
    );
  },
  getWordMeaning: (word, from, cb) => {
    backensterApi.getWordMeaning(
      word,
      from,
      withReport(word.length, 'lexical_meaning', from, cb),
    );
  },
  sendProposal: (proposalText, from, cb) => {
    backensterApi.sendProposal(
      proposalText,
      from,
      withReport(proposalText.length, 'feedback', from, cb),
    );
  },
  fetchLatestSettings,
};
