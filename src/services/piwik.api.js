const request = require('request');
const config = require('./piwik.config');
const { objectToUrl, uuidv4 } = require('../helpers');

const api = request.defaults({
  baseUrl: config.baseUrl,
});

function sendPiwikReport(symbolsLen, action, lang, callback) {
  api.get({
    url: `${config.piwikReportUrl}?${objectToUrl({
      idsite: 20,
      rec: 1,
      apiv: 1,
      r: Math.floor(Math.random() * (99999999 - 1 + 1) + 1),
      uid: uuidv4(),
      _idts: Date.now(),
      e_c: action,
      e_a: lang,
      e_n: 'text',
      e_v: symbolsLen,
      url: 'https://lingvanex.com/vkbot',
    })}`,
  }, callback);
}

module.exports = {
  sendPiwikReport,
};
