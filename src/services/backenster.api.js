const request = require('request');
const config = require('./backenster.config');

const api = request.defaults({
  baseUrl: config.baseURL,
  headers: {
    Authorization: `Bearer ${process.env.BACKENSTER_TOKEN}`,
  },
  json: true,
});

function getLanguages(callback) {
  api.get({
    url: config.getLanguagesListURL,
  }, (err, res) => callback(err || res.body.err, res.body.result));
}

function translate(data, from, to, callback) {
  api.post({
    url: config.translateURL,
    body: { data, from, to },
  }, (err, res) => callback(err || res.body.err, res.body.result));
}

function getWordMeaning(word, from, callback) {
  api.post({
    url: config.getLexicalMeaningURL,
    body: { word, from },
  }, (err, res) => {
    if (err || res.body.err) {
      callback(err || res.body.err);
      return;
    }
    const { partsOfSpeech = {} } = res.body.result;
    const resultedStr = Object.keys(partsOfSpeech)
      .reduce(
        (str, part) => `${str + part}\n\t${partsOfSpeech[part].map(partObj => partObj.meaning)}\n`,
        '',
      );
    callback(null, resultedStr);
  });
}

function sendProposal(message, language, callback) {
  api.post({
    url: config.proposalURL,
    body: {
      version: process.env.VERSION,
      language,
      message,
      appKey: process.env.APP_KEY,
    },
  }, (err, res) => callback(err || res.body.err, res.body.result));
}

module.exports = {
  translate,
  getLanguages,
  getWordMeaning,
  sendProposal,
};
