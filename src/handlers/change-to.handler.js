const { getLangCodeByName, getLanguageList } = require('../models/language.model');
const { updateUser } = require('../models/user.model');

module.exports = function changeToHandler(ctx, next) {
  const { currentUser, message: { body: message }, getLocaleString } = ctx;
  const langName = message.split(' ').slice(1).join(' ');
  const to = getLangCodeByName(langName);
  if (to) {
    updateUser(currentUser.userId, { to });
    ctx.reply(getLocaleString('LANGUAGE_WAS_CHANGED', { language: langName }));
  } else {
    ctx.reply([
      getLanguageList().join('\n'),
      getLocaleString('SELECT_LANGUAGE'),
    ].join('\n\n'));
  }
  next();
};
