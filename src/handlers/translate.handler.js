const { updateUser } = require('../models/user.model');
const { addWordToLearn } = require('../models/training.model');
const { translate } = require('../services');

module.exports = function translateHandler(ctx, next) {
  const {
    currentUser,
    getLocaleString,
    message: { body: message },
    settings,
  } = ctx;
  const textToTranslate = message.slice(0, currentUser.freeSymbols);
  if (textToTranslate.length > 0
    && settings.CharacterCountRequestLimitFree >= textToTranslate.length
    && settings.TranslationDayLimitRequestCount > currentUser.requestsForBot
  ) {
    updateUser(currentUser.userId, {
      freeSymbols: currentUser.freeSymbols - textToTranslate.length,
    });
    translate(textToTranslate, currentUser, (err, res) => {
      const translated = res.split('\n')[0];
      if (textToTranslate.split(' ').length === 1) {
        addWordToLearn(currentUser.userId, {
          word: textToTranslate,
          translated,
          langTo: currentUser.to,
          langFrom: currentUser.from,
        });
      }
      ctx.reply(res);
      next();
    });
  } else {
    ctx.reply(getLocaleString('LIMIT_ERROR'));
    next();
  }
};
