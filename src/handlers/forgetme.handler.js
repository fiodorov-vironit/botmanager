const { resetInterval } = require('../models/wakeup-interval.model');

module.exports = function forgetmeHandler(ctx, next) {
  const { currentUser, getLocaleString } = ctx;
  resetInterval(currentUser.userId);
  ctx.reply(getLocaleString('USER_WAKEUP_STOP_MESSAGE'));
  next();
};
