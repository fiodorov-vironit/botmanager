const { getLangCodeByName, getLanguageList } = require('../models/language.model');
const { updateUser } = require('../models/user.model');

module.exports = function changeFromHandler(ctx, next) {
  const { currentUser, message: { body: message }, getLocaleString } = ctx;
  const langName = message.split(' ').slice(1).join(' ');
  const from = getLangCodeByName(langName);
  if (from) {
    updateUser(currentUser.userId, { from });
    ctx.reply(getLocaleString('LANGUAGE_WAS_CHANGED', { language: langName }));
  } else {
    ctx.reply([
      getLanguageList().join('\n'),
      getLocaleString('SELECT_LANGUAGE'),
    ].join('\n\n'));
  }
  next();
};
