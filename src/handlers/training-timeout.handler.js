const { MODES } = require('../config/constants');
const { updateUser } = require('../models/user.model');

module.exports = function trainingTimeoutHandler(ctx) {
  const { currentUser, getLocaleString } = ctx;
  if (currentUser.mode === MODES.TRAINING_MODE) {
    updateUser(currentUser.userId, {
      mode: MODES.TRANSLATE_MODE,
    });
    ctx.reply(getLocaleString('STOP_TRAINING_MODE'));
  }
};
