const { getLangNameByCode } = require('../models/language.model');

module.exports = function paramsHandler(ctx, next) {
  const { currentUser, getLocaleString } = ctx;
  ctx.reply([
    getLocaleString('YOUR_LANGUAGE', {
      language: getLangNameByCode(currentUser.from),
    }),
    getLocaleString('TRANSLATE_LANGUAGE', {
      language: getLangNameByCode(currentUser.to),
    }),
  ].join('\n'));
  next();
};
