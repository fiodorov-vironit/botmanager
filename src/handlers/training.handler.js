const { MODES, TRAINING_MODE_TIMEOUT } = require('../config/constants');
const { updateUser } = require('../models/user.model');
const { getNextWord, resetTimeout, cancelTimeout } = require('../models/training.model');
const { getLangNameByCode } = require('../models/language.model');
const trainingTimeoutHandler = require('./training-timeout.handler');

module.exports = function trainingHandler(ctx, next) {
  const { currentUser, getLocaleString, message: { body: message } } = ctx;
  const { wordToLearn } = currentUser;
  const nextWord = getNextWord(currentUser.userId);
  if (wordToLearn) {
    if (message.toLowerCase() === wordToLearn.translated.toLowerCase()) {
      ctx.reply(getLocaleString('RIGHT'));
    } else {
      ctx.reply(getLocaleString('INCORRECT_ANSWER', { answer: wordToLearn.translated }));
    }
    if (nextWord) {
      ctx.reply(`\n\n${getLocaleString('NEXT')}`);
    }
  }
  if (nextWord) {
    resetTimeout(
      currentUser.userId,
      setTimeout(() => trainingTimeoutHandler(ctx), TRAINING_MODE_TIMEOUT),
    );
    ctx.currentUser = updateUser(currentUser.userId, {
      wordToLearn: nextWord,
    });
    ctx.reply(getLocaleString('HOW_IT', {
      word: nextWord.word,
      language: getLangNameByCode(nextWord.langTo),
    }));
  } else {
    cancelTimeout(currentUser.userId);
    ctx.currentUser = updateUser(currentUser.userId, {
      mode: MODES.TRANSLATE_MODE,
      wordToLearn: null,
    });
    ctx.reply([
      getLocaleString('STOP_TRAINING_MODE'),
      getLocaleString('NO_WORDS_FOR_LEARN'),
    ].join('. '));
  }
  next();
};
