const { getWordMeaning } = require('../services');
const logger = require('../logger');

module.exports = function getMeaningHandler(ctx, next) {
  const { currentUser, getLocaleString, message: { body: message } } = ctx;
  const word = message.split(' ')[1];
  if (word) {
    getWordMeaning(word, currentUser.from, (err, res) => {
      if (err) {
        logger.error(err);
        return;
      }
      ctx.reply(res);
      next();
    });
  } else {
    ctx.reply(getLocaleString('INCORRECT_COMMAND_USAGE_ERROR'));
    next();
  }
};
