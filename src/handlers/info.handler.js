module.exports = function infoHandler(ctx, next) {
  const { getLocaleString } = ctx;
  ctx.reply([
    getLocaleString('FIRST_INFO'),
    getLocaleString('SECOND_INFO'),
  ].join('\n\n'));
  next();
};
