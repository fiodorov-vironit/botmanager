const { MODES } = require('../config/constants');
const { updateUser } = require('../models/user.model');
const { cancelTimeout } = require('../models/training.model');

module.exports = function stopTrainingModeHandler(ctx, next) {
  const { currentUser, getLocaleString } = ctx;
  if (currentUser.mode === MODES.TRAINING_MODE) {
    cancelTimeout(currentUser.userId);
    updateUser(currentUser.userId, {
      mode: MODES.TRANSLATE_MODE,
    });
    ctx.reply(getLocaleString('STOP_TRAINING_MODE'));
  }
  next();
};
