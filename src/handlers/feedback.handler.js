const { sendProposal } = require('../services');
const logger = require('../logger');

module.exports = function feedbackHandler(ctx, next) {
  const { currentUser, getLocaleString, message: { body: message } } = ctx;
  const feedbackMessage = message.split(' ').slice(1).join(' ');
  if (feedbackMessage.length === 0) {
    ctx.reply(getLocaleString('INCORRECT_COMMAND_USAGE_ERROR'));
    next();
  } else {
    sendProposal(feedbackMessage, currentUser.locale, (err) => {
      if (err) {
        logger.error(err);
      } else {
        ctx.reply(getLocaleString('PROPOSAL_SEND'));
      }
      next();
    });
  }
};
