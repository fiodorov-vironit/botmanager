const { getLanguageList } = require('../models/language.model');

module.exports = function languagesHandler(ctx, next) {
  ctx.reply(getLanguageList().join('\n'));
  next();
};
