const { MODES, TRAINING_MODE_TIMEOUT } = require('../config/constants');
const { updateUser } = require('../models/user.model');
const { getLangNameByCode } = require('../models/language.model');
const { getNextWord, resetTimeout } = require('../models/training.model');
const trainingTimeoutHandler = require('./training-timeout.handler');

module.exports = function startLearnWordsHandler(ctx, next) {
  const { currentUser, getLocaleString } = ctx;
  ctx.currentUser = updateUser(currentUser.userId, {
    mode: MODES.TRAINING_MODE,
  });
  const nextWord = currentUser.wordToLearn || getNextWord(currentUser.userId);
  if (nextWord) {
    resetTimeout(
      currentUser.userId,
      setTimeout(() => trainingTimeoutHandler(ctx), TRAINING_MODE_TIMEOUT),
    );
    ctx.currentUser = updateUser(currentUser.userId, {
      wordToLearn: nextWord,
    });
    ctx.reply(getLocaleString('HOW_IT', {
      word: nextWord.word,
      language: getLangNameByCode(nextWord.langTo),
    }));
  } else {
    ctx.currentUser = updateUser(currentUser.userId, {
      mode: MODES.TRANSLATE_MODE,
      wordToLearn: null,
    });
    ctx.reply(getLocaleString('NO_WORDS_FOR_LEARN'));
  }
  next();
};
