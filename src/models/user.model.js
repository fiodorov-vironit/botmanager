const store = require('../store');

function addUser(userId) {
  const { users } = store;
  const user = {
    userId,
    locale: process.env.DEFAULT_LOCALE,
    notificationsDisabled: false,
  };
  [user.from, user.to] = process.env.DEFAULT_PAIR.split(',');
  users[userId] = user;
  return users[userId];
}

function updateUser(userId, newParams) {
  const { users } = store;
  users[userId] = {
    ...users[userId],
    ...newParams,
  };
  return users[userId];
}

function getUser(userId) {
  const { users } = store;
  return users[userId] || addUser(userId);
}

module.exports = {
  getUser,
  updateUser,
};
