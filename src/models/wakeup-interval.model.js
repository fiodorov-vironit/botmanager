const store = require('../store');

function resetInterval(userId) {
  const { wakeupIntervals } = store;
  if (wakeupIntervals[userId]) {
    clearInterval(wakeupIntervals[userId]);
    delete wakeupIntervals[userId];
  }
}

exports.resetInterval = resetInterval;

exports.updateInterval = function updateInterval(userId, interval) {
  const { wakeupIntervals } = store;
  resetInterval(userId);
  wakeupIntervals[userId] = interval;
};
