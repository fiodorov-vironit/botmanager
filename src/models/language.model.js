const store = require('../store');

exports.getLangNameByCode = langCode => store.languages[langCode];
exports.getLangCodeByName = langName => (
  Object.keys(store.languages)
    .find(langCode => store.languages[langCode] === langName)
);
exports.getLanguageList = () => Object.values(store.languages);
