const store = require('../store');

function getTraining(userId) {
  const { training } = store;
  training[userId] = training[userId] || { words: [] };
  return training[userId];
}
function addWordToLearn(userId, wordInfo) {
  const training = getTraining(userId);
  training.words.push(wordInfo);
}

function getNextWord(userId) {
  const training = getTraining(userId);
  return training.words.shift();
}

function cancelTimeout(userId) {
  const training = getTraining(userId);
  if (training.timeout) {
    clearTimeout(training.timeout);
  }
}

function resetTimeout(userId, timeout) {
  const training = getTraining(userId);
  cancelTimeout(userId);
  training.timeout = timeout;
}

module.exports = {
  addWordToLearn,
  getNextWord,
  resetTimeout,
  cancelTimeout,
};
