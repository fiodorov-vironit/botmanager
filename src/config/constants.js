exports.RESET_INTERVAL = 1000 * 60 * 60 * 24;
exports.TRAINING_MODE_TIMEOUT = 1000 * 60 * 5;
exports.MODES = {
  TRANSLATE_MODE: 'translate',
  TRAINING_MODE: 'training',
};
