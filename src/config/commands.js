const { MODES } = require('./constants');
const handlers = require('../handlers');

const commands = {
  forgetme: {
    text: '/forgetme',
    handler: handlers.forgetmeHandler,
  },
  languages: {
    text: '/languages',
    handler: handlers.languagesHandler,
  },
  changeFrom: {
    text: '/changeFrom',
    handler: handlers.changeFromHandler,
  },
  changeTo: {
    text: '/changeTo',
    handler: handlers.changeToHandler,
  },
  params: {
    text: '/params',
    handler: handlers.paramsHandler,
  },
  info: {
    text: '/info',
    handler: handlers.infoHandler,
  },
  getMeaning: {
    text: '/getMeaning',
    handler: handlers.getMeaningHandler,
  },
  startLearnWords: {
    text: '/startLearnWords',
    handler: handlers.startLearnWordsHandler,
  },
  stopTrainingMode: {
    text: '/stopTrainingMode',
    handler: handlers.stopTrainingModeHandler,
  },
  feedback: {
    text: '/feedback',
    handler: handlers.feedbackHandler,
  },
};

const inputHandlers = {
  [MODES.TRANSLATE_MODE]: handlers.translateHandler,
  [MODES.TRAINING_MODE]: handlers.trainingHandler,
};

module.exports = {
  commands,
  inputHandlers,
};
