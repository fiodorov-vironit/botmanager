const { inputHandlers } = require('../config/commands');

module.exports = function userInputMiddleware(ctx, next) {
  const { currentUser } = ctx;
  const handler = inputHandlers[currentUser.mode];
  return handler(ctx, next);
};
