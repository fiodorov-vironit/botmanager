const { daysToMs } = require('../helpers');
const { updateInterval } = require('../models/wakeup-interval.model');

module.exports = function wakeupMiddleware(ctx, next) {
  const { currentUser, settings, getLocaleString } = ctx;
  if (!currentUser.notificationsDisabled) {
    updateInterval(
      currentUser.userId,
      setInterval(
        () => ctx.reply(getLocaleString('USER_WAKEUP_MESSAGE')),
        daysToMs(settings.UserWakeUpInterval),
      ),
    );
  }
  next();
};
