const { t } = require('../locales');

module.exports = function localeMiddleware(ctx, next) {
  const { currentUser: { locale } } = ctx;
  ctx.getLocaleString = t.bind(null, locale);
  next();
};
