const { getUser, updateUser } = require('../models/user.model');
const { RESET_INTERVAL, MODES } = require('../config/constants');

module.exports = function userMiddleware(ctx, next) {
  const { message: { user_id: userId }, settings } = ctx;
  ctx.currentUser = getUser(userId);
  if (!ctx.currentUser.expired || ctx.currentUser.expired < Date.now()) {
    ctx.currentUser = updateUser(userId, {
      expired: Date.now() + RESET_INTERVAL,
      freeSymbols: settings.TranslationDayLimitCharacterCount,
      requestsForBot: 0,
      mode: MODES.TRANSLATE_MODE,
    });
  }
  ctx.currentUser = updateUser(userId, {
    requestsForBot: ctx.currentUser.requestsForBot + 1,
  });
  next();
};
