const { fetchLatestSettings } = require('../services/app-settings.api');
const { getInt } = require('../helpers');

const defaultSettings = {
  TranslationDayLimitCharacterCount: 100,
  CharacterCountRequestLimitFree: 1000,
  TranslationDayLimitRequestCount: 10000,
  EmailRequestInterval: 80,
  FacebookLikeInterval: 100,
  StoreReviewRequestInterval: 100,
  FeedbackRequestInterval: 40,
  UserWakeUpInterval: 1,
};

module.exports = function appSettingsMiddleware(ctx, next) {
  fetchLatestSettings((err, settings) => {
    if (err) {
      ctx.settings = defaultSettings;
      next();
      return;
    }
    ctx.settings = Object.keys(settings)
      .reduce((result, settingName) => ({
        ...result,
        [settingName]: getInt(settings[settingName]),
      }), defaultSettings);
    next();
  });
};
