const { commands } = require('../config/commands');

module.exports = function reminderMiddleware(ctx) {
  const { currentUser, settings, getLocaleString } = ctx;
  if (currentUser.requestsForBot % settings.FeedbackRequestInterval === 0) {
    ctx.reply(`\n\n${getLocaleString('FEEDBACK_REQUEST_MESSAGE', { cmd_feedback: commands.feedback.text })}`);
  }
};
